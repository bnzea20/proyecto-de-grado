FROM python:3.9.6

LABEL CRISTIAN_ZAMBRANO&BRAYAN_ZEA cczambrano02@ucatolica.edu.co

ENV PYTHONUNBUFFERED 1
RUN mkdir /code

WORKDIR /code
COPY . /code/

RUN pip install -r requirements.txt

CMD ["gunicorn", "-c", "config/gunicorn/conf.py", "--bind", ":8000", "--chdir", "docker_django", "docker_django.wsgi:application"]