from django.db import models
from django.db.models.fields import AutoField

class Cualidades(models.Model):
    id_cualidad=models.AutoField(primary_key=True)
    nombre_cualidad =models.CharField(max_length=30)
    descripcion = models.TextField()
    def __str__(self):
        return self.id_cualidad


class Titulo_Academico(models.Model):
    id=models.AutoField(primary_key=True)
    titulo=models.TextField()
    institucion=models.TextField()
    fecha_graduacion=models.DateField()
    def __str__(self):
        return self.id

class Estado_civil(models.Model):
    id_estado_civil= models.AutoField(primary_key=True)
    estado=models.CharField(max_length=30)
    def __str__(self):
        return self.id_estado_civil

class Persona(models.Model):
    cedula=models.BigIntegerField(primary_key=True)
    nombre=models.CharField(max_length=30)
    apellidos=models.CharField(max_length=30)
    fecha_nacimiento=models.DateField()
    correo=models.EmailField()
    genero=models.BooleanField()
    id_estado_civil=models.ForeignKey(Estado_civil, on_delete=models.CASCADE)
    id_titulo=models.ForeignKey(Titulo_Academico,on_delete=models.CASCADE)
    perfil=models.TextField()
    def __str__(self):
        return self.cedula

class Persona_Cualidades(models.Model):
    id_persona=models.ForeignKey(Persona, on_delete=models.CASCADE)
    id_cualidad=models.ForeignKey(Cualidades, on_delete=models.CASCADE)

class Persona_Referencia(models.Model):
    cedula=models.BigIntegerField(primary_key=True)
    nombre=models.CharField(max_length=30)
    apellidos=models.CharField(max_length=30)
    id_persona_refiere=models.ForeignKey(Persona, on_delete=models.CASCADE)
    def __str__(self):
        return self.cedula

class Empresa(models.Model):
    id_empresa=models.AutoField(primary_key=True)
    nombre=models.TextField()
    def __str__(self):
        return self.id_empresa

class Cargo(models.Model):
    id_cargo =models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    def __str__(self):
        return self.id_cargo


class Persona_Empresa_Cargo(models.Model):
    id_persona=models.ForeignKey(Persona,on_delete=models.CASCADE)
    id_cargo=models.ForeignKey(Cargo, on_delete=models.CASCADE)
    id_empresa=models.ForeignKey(Empresa,on_delete=models.CASCADE)
    fecha_inicio=models.DateField()
    fecha_fin=models.DateField()

class Celular(models.Model):
    id_celular=models.AutoField(primary_key=True)
    numero=models.BigIntegerField()
    id_propietario=models.ForeignKey(Persona,on_delete=models.CASCADE)
    id_empresa=models.ForeignKey(Empresa,on_delete=models.CASCADE)
    def __str__(self):
        return self.id_celular

