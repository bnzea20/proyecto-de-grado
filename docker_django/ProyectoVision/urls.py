from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


from ProyectoVision.views import inicio, elinstituto, aelinstituto, participa, aparticipa, ainicio, aformulario, formulario, blog, ablog, atencion, aatencion


urlpatterns = [
    path('', inicio, name='inicio'),
    path('inicio/', inicio, name='inicio'),
    path('ainicio/', ainicio, name='ainicio'),
    path('elinstituto/', elinstituto, name='elinstituto'),
    path('aelinstituto/', aelinstituto, name='aelinstituto'),
    path('participa/', participa, name='participa'),
    path('aparticipa/', aparticipa, name='aparticipa'),
    path('aformulario/', aformulario, name='aformulario'),
    path('formulario/', formulario, name='formulario'),
    path('blog/', blog, name='blog'),
    path('ablog/', ablog, name='ablog'),
    path('atencion/', atencion, name='atencion'),
    path('aatencion/', aatencion, name='aatencion'),
]
