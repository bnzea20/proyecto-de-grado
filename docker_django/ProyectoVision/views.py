from docker_django.settings import BASE_DIR
from django.http import HttpResponse
from django.http import HttpRequest
from django.shortcuts import render
from django.template import Template, Context
from .models import Cualidades, Titulo_Academico, Estado_civil, Persona, Persona_Cualidades, Persona_Referencia, Empresa, Cargo, Persona_Empresa_Cargo, Celular
import os


def inicio(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'noAccesible/inicio.html',
        {}
    )


def ainicio(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'Accesible/Ainicio.html',
        {}
    )


def formulario(request):
    assert isinstance(request, HttpRequest)
    vmensajeOK=""
    vnombre = ""
    verror = ""
    vchec1 = 0
    vchec2 = 0
    vchec3 = 0
    vchec4 = 0
    if request.method == "POST":
        vnombre = request.POST['nombre']
        vapellidos = request.POST['apellidos']
        vnumero = request.POST['numero']
        vcedula = request.POST['cedula']
        vcorreo = request.POST['correo']
        if len(vapellidos.split(" ")) < 2:
            verror += " No es posible registrar el nombre con un solo apellido."
        if len(vnumero) != 10:
            verror += " El telefono debe contener 10 numeros."
        if request.POST.get('chec1'):
            vchec1 = 1
        if request.POST.get('chec2'):
            vchec2 = 1
        if request.POST.get('chec3'):
            vchec3 = 1
        if request.POST.get('chec4'):
            vchec4 = 1
        if request.POST.get('genero'):
            v1 = 0
        else:
            verror += " El campo genero esta vacio."
        if vchec1 == 0 and vchec2 == 0 and vchec3 == 0 and vchec4 == 0:
            verror += " Seleccione por lo menos una cualidad."
        if len(vcedula) < 7 or len(vcedula) > 10:
            verror += " La cedula debe contener entre 7 y 10 numeros."
        if str(request.POST['final']) < str(request.POST['inicio']):
            verror += " La fecha final no puede ser menor a la fecha inicial."
        if vcorreo.find("@") == -1 or vcorreo.find(".") == -1:
            verror += " El correo debe contener un '@' y un '.'"
        if request.POST['genero'] == 'masculino':
            vgenero=1
        else:
            vgenero=0
        if request.POST['civil'] =="Soltero":
            vcivil=1
        if request.POST['civil'] =="Casado":
            vcivil=2
        if request.POST['civil'] =="Divorciado":
            vcivil=3
        if request.POST['civil'] =="Viudo":
            vcivil=4
        if request.POST['civil'] =="Union libre":
            vcivil=5
        if verror == "":

            tit=Titulo_Academico()
            tit.titulo=request.POST['titulo']
            tit.fecha_graduacion=request.POST['graduacion']
            tit.institucion=request.POST['institucion']
            tit.save()
            pers=Persona()
            pers.nombre=vnombre
            pers.apellidos=vapellidos
            pers.cedula=vcedula
            pers.correo=vcorreo
            pers.fecha_nacimiento=request.POST['nacimiento']
            pers.id_estado_civil = Estado_civil.objects.get(id_estado_civil=vcivil)
            pers.genero= vgenero
            pers.id_titulo= tit
            pers.perfil=request.POST['perfil']
            pers.save()

            if vchec1 == 1:
                per_cua=Persona_Cualidades()
                per_cua.id_persona=pers
                per_cua.id_cualidad=Cualidades.objects.get(id_cualidad=vchec1)
                per_cua.save()

            if vchec2 == 1:
                per_cua2=Persona_Cualidades()
                per_cua2.id_persona=pers
                per_cua2.id_cualidad=Cualidades.objects.get(id_cualidad=vchec2)
                per_cua2.save()

            if vchec3 == 1:
                per_cua3=Persona_Cualidades()
                per_cua3.id_persona=pers
                per_cua3.id_cualidad=Cualidades.objects.get(id_cualidad=vchec3)
                per_cua3.save()

            if vchec4 == 1:
                per_cua4=Persona_Cualidades()
                per_cua4.id_persona=pers
                per_cua4.id_cualidad=Cualidades.objects.get(id_cualidad=vchec4)
                per_cua4.save()

            per_ref=Persona_Referencia()
            per_ref.cedula=request.POST['refcedula']
            per_ref.nombre=request.POST['refnombre']
            per_ref.apellidos=request.POST['refapellido']
            per_ref.id_persona_refiere=pers
            per_ref.save()

            emp=Empresa()
            emp.nombre=request.POST['empresa']
            emp.save()

            car=Cargo()
            car.nombre=request.POST['cargo']
            car.save()

            Per_Emp_Car=Persona_Empresa_Cargo()
            Per_Emp_Car.id_persona=pers
            Per_Emp_Car.id_cargo=car
            Per_Emp_Car.id_empresa=emp
            Per_Emp_Car.fecha_inicio=request.POST['inicio']
            Per_Emp_Car.fecha_fin=request.POST['final']
            Per_Emp_Car.save()

            cel=Celular()
            cel.numero=vnumero
            cel.id_propietario=pers
            cel.save()

            cel1=Celular()
            cel1.numero=request.POST['telefono']
            cel1.id_empresa=emp
            cel1.save()
            vmensajeOK = "Registro realizado correctamente"
            return render(
                request,
                'noAccesible/formulario.html',
                {
                    'title': 'Resultado',
                    'message': 'Resultado de la acción es OK.',
                    'nombre': vnombre,
                    'mensajeOK': vmensajeOK,
                }
            )
    return render(
        request,
        'noAccesible/formulario.html',
        {
            'title': 'Hoja de vida',
            'message': 'Your contact page.',
            'nombre': vnombre,
            'error': verror,
        }
    )


def aformulario(request):
    assert isinstance(request, HttpRequest)
    vmensajeOK=""
    vnombre = ""
    verror = ""
    vchec1 = 0
    vchec2 = 0
    vchec3 = 0
    vchec4 = 0
    if request.method == "POST":
        vnombre = request.POST['nombre']
        vapellidos = request.POST['apellidos']
        vnumero = request.POST['numero']
        vcedula = request.POST['cedula']
        vcorreo = request.POST['correo']
        if len(vapellidos.split(" ")) < 2:
            verror += " No es posible registrar el nombre con un solo apellido."
        if len(vnumero) != 10:
            verror += " El telefono debe contener 10 numeros."
        if request.POST.get('chec1'):
            vchec1 = 1
        if request.POST.get('chec2'):
            vchec2 = 1
        if request.POST.get('chec3'):
            vchec3 = 1
        if request.POST.get('chec4'):
            vchec4 = 1
        if request.POST.get('genero'):
            v1 = 0
        else:
            verror += " El campo genero esta vacio."
        if vchec1 == 0 and vchec2 == 0 and vchec3 == 0 and vchec4 == 0:
            verror += " Seleccione por lo menos una cualidad."
        if len(vcedula) < 7 or len(vcedula) > 10:
            verror += " La cedula debe contener entre 7 y 10 numeros."
        if str(request.POST['final']) < str(request.POST['inicio']):
            verror += " La fecha final no puede ser menor a la fecha inicial."
        if vcorreo.find("@") == -1 or vcorreo.find(".") == -1:
            verror += " El correo debe contener un '@' y un punto."
        if request.POST['genero'] == 'masculino':
            vgenero=1
        else:
            vgenero=0
        if request.POST['civil'] =="Soltero":
            vcivil=1
        if request.POST['civil'] =="Casado":
            vcivil=2
        if request.POST['civil'] =="Divorciado":
            vcivil=3
        if request.POST['civil'] =="Viudo":
            vcivil=4
        if request.POST['civil'] =="Union libre":
            vcivil=5
        if verror == "":

            tit=Titulo_Academico()
            tit.titulo=request.POST['titulo']
            tit.fecha_graduacion=request.POST['graduacion']
            tit.institucion=request.POST['institucion']
            tit.save()
            pers=Persona()
            pers.nombre=vnombre
            pers.apellidos=vapellidos
            pers.cedula=vcedula
            pers.correo=vcorreo
            pers.fecha_nacimiento=request.POST['nacimiento']
            pers.id_estado_civil = Estado_civil.objects.get(id_estado_civil=vcivil)
            pers.genero= vgenero
            pers.id_titulo= tit
            pers.perfil=request.POST['perfil']
            pers.save()

            if vchec1 == 1:
                per_cua=Persona_Cualidades()
                per_cua.id_persona=pers
                per_cua.id_cualidad=Cualidades.objects.get(id_cualidad=vchec1)
                per_cua.save()

            if vchec2 == 1:
                per_cua2=Persona_Cualidades()
                per_cua2.id_persona=pers
                per_cua2.id_cualidad=Cualidades.objects.get(id_cualidad=vchec2)
                per_cua2.save()

            if vchec3 == 1:
                per_cua3=Persona_Cualidades()
                per_cua3.id_persona=pers
                per_cua3.id_cualidad=Cualidades.objects.get(id_cualidad=vchec3)
                per_cua3.save()

            if vchec4 == 1:
                per_cua4=Persona_Cualidades()
                per_cua4.id_persona=pers
                per_cua4.id_cualidad=Cualidades.objects.get(id_cualidad=vchec4)
                per_cua4.save()

            per_ref=Persona_Referencia()
            per_ref.cedula=request.POST['refcedula']
            per_ref.nombre=request.POST['refnombre']
            per_ref.apellidos=request.POST['refapellido']
            per_ref.id_persona_refiere=pers
            per_ref.save()

            emp=Empresa()
            emp.nombre=request.POST['empresa']
            emp.save()

            car=Cargo()
            car.nombre=request.POST['cargo']
            car.save()

            Per_Emp_Car=Persona_Empresa_Cargo()
            Per_Emp_Car.id_persona=pers
            Per_Emp_Car.id_cargo=car
            Per_Emp_Car.id_empresa=emp
            Per_Emp_Car.fecha_inicio=request.POST['inicio']
            Per_Emp_Car.fecha_fin=request.POST['final']
            Per_Emp_Car.save()

            cel=Celular()
            cel.numero=vnumero
            cel.id_propietario=pers
            cel.id_empresa=emp
            cel.save()

            vmensajeOK = "Registro realizado correctamente"
            return render(
                request,
                'Accesible/Aformulario.html',
                {
                    'title': 'Resultado',
                    'message': 'Resultado de la acción es OK.',
                    'nombre': vnombre,
                    'mensajeOK': vmensajeOK,
                }
            )
    return render(
        request,
        'Accesible/Aformulario.html',
        {
            'title': 'Hoja de vida',
            'message': 'Your contact page.',
            'nombre': vnombre,
            'error': verror,
        }
    )


def elinstituto(request):
    return render(
        request,
        'noAccesible/elinstituto.html',
        {}
    )


def blog(request):
    return render(
        request,
        'noAccesible/blog.html',
        {}
    )


def ablog(request):
    return render(
        request,
        'Accesible/Ablog.html',
        {}
    )


def aelinstituto(request):
    return render(
        request,
        'Accesible/Aelinstituto.html',
        {}
    )


def participa(request):
    return render(
        request,
        'noAccesible/participa.html',
        {}
    )


def aparticipa(request):
    return render(
        request,
        'Accesible/Aparticipa.html',
        {}
    )


def atencion(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'noAccesible/atencion.html',
        {}
    )


def aatencion(request):
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'Accesible/Aatencion.html',
        {}
    )